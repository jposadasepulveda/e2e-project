# Practica E2E pruebas de software

Para la siguiente practica se ha escogido la plataforma de Youtube como lugar para correr algunas test e2e y smoke, esto con la finalida de
establecer un plan de pruebas que conste de los flujos principales de un usuario comun y validar en los distintos casos que la plataforma
actue de la manera que se espera

## Instalación del proyecto

clonando el repositorio

`$ git clone url`

Instalando dependencias desde npm

`$ npm install`

## Cómo se usa

Para correr los scripts desde la plataforma de cypress
`$ npm run cypress:open`

Para correr los scripts desde console
`$ npm run "cy:run`

## Informacion del proyecto

### cypress folder
Creacion por defecto de cypress, aqui se tendran todos los archivos necesarios para los scripts tales como fixtures, plugins, elementos de support (funciones, custom commands, etc)

### integration
Aqui tendremos los archivos correspondientes a los scripts automatizados

#### Escenario 1. Smoke a la vista principal 

| Caso de prueba      | Smoke test main page                                                           |
| -----------         | -----------                                                                    |
| Tipo                | Smoke                                                                          |
| Version             | 1.0                                                                            |
| Descripcion         | Revision de la pagina principal                                                |
| Pre-condiciones     | 1. Intercept a la peticion GET que carga las tablas  2.Visitar la pagina       |
| Pasos secuenciales  | 1. ir a la pagina principal 2. validar header 3. validar body tabla de cryptos |

#### Escenario 2. 

| Caso de prueba      | Filter - Only Should render Crypto                                                                                 |
| -----------         | -----------                                                                                                        |
| Tipo                | e2e                                                                                                                |
| Version             | 1.0                                                                                                                |
| Descripcion         | Validar el search input de la tabla                                                                                |
| Pre-condiciones     | 1. Intercept a la peticion GET que carga las tablas  2.Visitar la pagina 3. obtener el input y tratar de filtrar   |
| Pasos secuenciales  | 1. ir a la pagina principal 2. escribir una cryptos en el input 3. validar la informacion mostrada                  |

#### Escenario 3. 

| Caso de prueba      | Bitcoin Detail Validations                                                                                                                |
| -----------         | -----------                                                                                                                               |
| Tipo                | e2e                                                                                                                                       |
| Version             | 1.0                                                                                                                                       |
| Descripcion         | Validar el boton de detalles en la tabla principal y la nueva pagina                                                                      |
| Pre-condiciones     | 1. Intercept a la peticion GET que carga las tablas  2.Visitar la pagina 3. obtener el boton y clickearlo                                 |
| Pasos secuenciales  | 1. ir a la pagina principal 2. dar click en detalles 3. validar la informacion de la nueva pagina (resumen, grafica, ofertas de cambio)   |