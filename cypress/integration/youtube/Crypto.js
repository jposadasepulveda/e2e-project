export default class Crypto {
  visite() {
    cy.get(".shadow.w-screen")
      .should("be.visible")
      .within(function () {
        cy.get("#image0");
        cy.get(".flex > .font-semibold");
        cy.get(".justify-between > .hidden")
          .should("be.visible")
          .within(function () {
            cy.get('.text-sm > [href="/coin/bitcoin"]').should("be.visible");
            cy.get('.text-sm > [href="/coin/ethereum"]').should("be.visible");
            cy.get('[href="/coin/ripple"]').should("be.visible");
          });
      });
    cy.get(".container")
      .should("be.visible")
      .within(function () {
        cy.get("table")
          .should("be.visible")
          .within(function () {
            cy.get("thead > tr > th")
              .should("be.visible")
              .each(function (item) {
                expect(item).to.be.visible;
              });
            cy.get("tbody > tr")
              .should("be.visible")
              .each(function (item) {
                expect(item).to.be.visible;
              });
          });
      });
  }

  search(input) {
    cy.get("#filter")
      .type(input)
      .then(() => {
        cy.get(".container")
          .should("be.visible")
          .within(function () {
            cy.get("table")
              .should("be.visible")
              .within(function () {
                cy.get("thead > tr > th")
                  .should("be.visible")
                  .each(function (item) {
                    expect(item).to.be.visible;
                  });
                cy.get("tbody > tr")
                  .should("be.visible")
                  .each(function (item) {
                    expect(item).to.be.visible;
                    cy.get(item).should("contain", "Ethereum");
                  });
              });
          });
      });
  }

  bitcoinDetail() {
    cy.get(".container").within(function () {
      cy.get("table").within(function () {
        cy.get("tbody > tr")
          .eq(0)
          .within(function () {
            cy.get("button").should("be.visible").and("be.enabled").click();
            cy.wait("@bitcoin_history");
          });
      });
    });
    cy.get(".flex.flex-col.items-center").within(function () {
      cy.get(".items-center").within(function () {
        cy.get(".w-20").should("be.visible");
        cy.get(".text-5xl").should("be.visible");
      });
      cy.get("ul > li")
        .should("be.visible")
        .each(function (item) {
          expect(item).to.be.visible;
        });
      cy.get(".bg-green-500").should("be.visible").and("be.enabled");
      cy.get("input").should("be.visible");
      cy.get("span").should("be.visible");
    });
    cy.get(".chartjs-render-monitor").should("be.visible");
    cy.get(".text-xl.my-10").should("be.visible");
    cy.get("table > tr")
      .should("be.visible")
      .each(function (item) {
        expect(item).to.be.visible;
      });
  }
}
