/// <reference types="cypress" />
import Crypto from "./Crypto";

const page = new Crypto();

describe("Cryptos's home page!", () => {
  beforeEach(function () {
    cy.intercept("GET", "https://api.coincap.io/v2/assets*").as(
      "coin_petition"
    );
    cy.intercept("GET", "https://api.coincap.io/v2/assets/bitcoin/history*").as(
      "bitcoin_history"
    );
  });
  beforeEach(function () {
    cy.visit("https://crypto-money-app.herokuapp.com/");
    cy.wait("@coin_petition");
  });

  it("SMOKE TEST - Should render Crypto's Page", () => {
    page.visite();
  });

  it("Filter - Only Should render Crypto", () => {
    page.search("Ethereum");
  });

  it("Bitcoin Detail Validations", () => {
    page.bitcoinDetail();
  });
});
